#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    int32_t biWidth;
    int32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    int32_t biXPelsPerMeter;
    int32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERR
};
enum write_status  {
    WRITE_OK = 0,
    WRITE_ERR
};
enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, const struct image* img);

enum read_status bmp_read(const char* filename, struct image* img);
enum write_status bmp_write(const char* filename, struct image const* img);

#endif
