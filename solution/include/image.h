#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    int32_t width, height;
    struct pixel* data;
};

void free_memory_image(struct image* img);

#endif
