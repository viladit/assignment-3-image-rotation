#ifndef rotation_H
#define rotation_H

#include "image.h"

void adaptive_median_filter(struct image *input, int s_max);

#endif
