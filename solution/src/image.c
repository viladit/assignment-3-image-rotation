#include "../include/image.h"
#include <stdlib.h>

void free_memory_image(struct image *img) {
    free(img -> data);
    img->data = NULL;
}
