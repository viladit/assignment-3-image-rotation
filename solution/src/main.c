#include <stdio.h>
#include <stdlib.h>

#include "../include/bmp.h"
#include "../include/adaptiveFiltering.h"

int main(int argc, char* argv[]) {
    // Input validation
    if (argc != 4) {
        fprintf(stderr, "Wrong input format! \nCorrect format is: %s <input-image.bmp> <output-image.bmp> <max-core>\n", argv[0]);
        return 1;
    }

    int core = atoi(argv[3]);
    int validCore = 1;
    if (core > 20) {
        validCore = 0;
    }
    if (!validCore) {
        fprintf(stderr, "Error: Valid core must be lesser then 20.\n");
        return 1;
    }

    struct image input_image;
    enum read_status status_of_reading = bmp_read(argv[1], &input_image);
    if (status_of_reading != READ_OK) {
        fprintf(stderr, "Reading error\n");
        return 1;
    }

    // Adaptive Filtering
    adaptive_median_filter(&input_image, core);

    // File writing
    enum write_status status_of_writing = bmp_write(argv[2], &input_image);
    if (status_of_writing != WRITE_OK) {
        fprintf(stderr, "Writing error\n");
        free_memory_image(&input_image);
        return 1;
    }
    free_memory_image(&input_image);
    printf("Success!\n");

    return 0;
}
