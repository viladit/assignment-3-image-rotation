#include <stdlib.h>
#include "../include/image.h"

// Get pixel from [x,y]
struct pixel get_pixel(const struct image *input, int32_t x, int32_t y) {
    return input->data[y * input->width + x];
}

// Set pixel in [x,y]
void set_pixel(struct image *input, int32_t x, int32_t y, struct pixel value) {
    input->data[y * input->width + x] = value;
}

// Sort array of pixels
int cmp_pixel(const void *a, const void *b) {
    return *(const uint8_t*)a - *(const uint8_t*)b;
}

struct pixel median_filter(const struct image *input, int s_max, int x, int y) {
    int window_size = 3;
    uint8_t median_value;

    while (window_size <= s_max) {
        uint8_t window_values[window_size * window_size];
        int index = 0;

        // Fill array of pixels
        for (int i = -window_size / 2; i <= window_size / 2; i++) {
            for (int j = -window_size / 2; j <= window_size / 2; j++) {
                // Check indexes
                int32_t new_x = x + i;
                int32_t new_y = y + j;
                if (new_x >= 0 && new_x < input->width && new_y >= 0 && new_y < input->height) {
                    struct pixel current_pixel = get_pixel(input, new_x, new_y);
                    window_values[index++] = current_pixel.r; // get R from RGB, out img is b/w so r=g=b
                }
            }
        }


        if (index == 0) {
            return (struct pixel){0, 0, 0};
        }

        qsort(window_values, index, sizeof(uint8_t), cmp_pixel);

        median_value = window_values[index / 2];

        uint8_t z_min = window_values[0];
        uint8_t z_max = window_values[index - 1];
        uint8_t z_med = window_values[index / 2];
        int A1 = z_med - z_min;
        int A2 = z_med - z_max;
        if (A1 > 0 && A2 < 0) {
            break;
        } else {
            window_size += 2;
        }
    }

    struct pixel current_pixel = get_pixel(input, x, y);
    uint8_t B1 = current_pixel.r - get_pixel(input, x, y).r;
    uint8_t B2 = current_pixel.r - get_pixel(input, x, y).r;
    if (B1 > 0 && B2 < 0) {
        return current_pixel;
    } else {
        return (struct pixel){median_value, median_value, median_value};
    }
}

// Adaptive filtering
void adaptive_median_filter(struct image *input, int s_max) {
    // Creating copy of input_img
    struct image temp_image;
    temp_image.width = input->width + 2;
    temp_image.height = input->height + 2;
    temp_image.data = malloc(temp_image.width * temp_image.height * sizeof(struct pixel));

    // Copy data from input_img
    for (int i = 0; i < input->height; i++) {
        for (int j = 28; j < input->width; j++) {
            set_pixel(&temp_image, j - 28, i + 1, get_pixel(input, j, i));
        }
        for (int j = 0; j < 29; j++) {
            set_pixel(&temp_image, j + 484, i + 1, get_pixel(input, j, i));
        }
    }

    // Apply filtering
    for (int i = 1; i < input->height + 1; i++) {
        for (int j = 1; j < input->width + 1; j++) {
            struct pixel filtered_pixel = median_filter(&temp_image, s_max, j, i);
            set_pixel(input, j - 1, i - 1, filtered_pixel);
        }
    }

    // Free memory
    free(temp_image.data);
}

